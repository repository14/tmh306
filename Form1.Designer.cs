﻿namespace odev
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.düzenleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageOrtalamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709YöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeRenkKanalıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açıklıkDesaturationYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renkKanalıYöntemiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.düzenleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(880, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // düzenleToolStripMenuItem
            // 
            this.düzenleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.averageOrtalamaToolStripMenuItem,
            this.bT709YöntemiToolStripMenuItem,
            this.normalizeRenkKanalıToolStripMenuItem,
            this.açıklıkDesaturationYöntemiToolStripMenuItem,
            this.renkKanalıYöntemiToolStripMenuItem});
            this.düzenleToolStripMenuItem.Name = "düzenleToolStripMenuItem";
            this.düzenleToolStripMenuItem.Size = new System.Drawing.Size(77, 24);
            this.düzenleToolStripMenuItem.Text = "Düzenle";
            // 
            // averageOrtalamaToolStripMenuItem
            // 
            this.averageOrtalamaToolStripMenuItem.Name = "averageOrtalamaToolStripMenuItem";
            this.averageOrtalamaToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.averageOrtalamaToolStripMenuItem.Text = "Average(Ortalama) Yöntemi";
            // 
            // bT709YöntemiToolStripMenuItem
            // 
            this.bT709YöntemiToolStripMenuItem.Name = "bT709YöntemiToolStripMenuItem";
            this.bT709YöntemiToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.bT709YöntemiToolStripMenuItem.Text = "BT-709 Yöntemi";
            // 
            // normalizeRenkKanalıToolStripMenuItem
            // 
            this.normalizeRenkKanalıToolStripMenuItem.Name = "normalizeRenkKanalıToolStripMenuItem";
            this.normalizeRenkKanalıToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.normalizeRenkKanalıToolStripMenuItem.Text = "Normalize Renk Kanalı Yöntemi";
            // 
            // açıklıkDesaturationYöntemiToolStripMenuItem
            // 
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Name = "açıklıkDesaturationYöntemiToolStripMenuItem";
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.açıklıkDesaturationYöntemiToolStripMenuItem.Text = "Açıklık(Desaturation) Yöntemi";
            // 
            // renkKanalıYöntemiToolStripMenuItem
            // 
            this.renkKanalıYöntemiToolStripMenuItem.Name = "renkKanalıYöntemiToolStripMenuItem";
            this.renkKanalıYöntemiToolStripMenuItem.Size = new System.Drawing.Size(300, 26);
            this.renkKanalıYöntemiToolStripMenuItem.Text = "Renk Kanalı Yöntemi";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 400);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(447, 42);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 400);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 450);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem düzenleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageOrtalamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bT709YöntemiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeRenkKanalıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açıklıkDesaturationYöntemiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renkKanalıYöntemiToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

