﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odev
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dosya = new OpenFileDialog();
            dosya.Filter = "Resim dosyası |.jpg;.nef;.png| video|.avi| Tüm Dosyalar |.";
            dosya.Title = "";
            dosya.ShowDialog();
            string DosyaYolu = dosya.FileName;
            pictureBox1.ImageLocation = DosyaYolu;
        }

        
        private Bitmap averageOrtalamaYöntemi(Bitmap cikisresmi)
        {
            for (int i = 0; i < cikisresmi.Height - 1; i++)
            {
                for (int j = 0; j < cikisresmi.Width - 1; j++)
                {
                    int deger = (cikisresmi.GetPixel(j, i).R + cikisresmi.GetPixel(j, i).G + cikisresmi.GetPixel(j, i).B * 0) / 3;
                    Color renk;
                    renk = Color.FromArgb(deger, deger, deger);
                    cikisresmi.SetPixel(j, i, renk);
                }
            }
            return cikisresmi;

        }

        private void averageOrtalamaYöntemiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = averageOrtalamaYöntemi(image);
            pictureBox2.Image = gri;
        }
        

        private Bitmap bT709Yöntemi(Bitmap cikisresmi)

        {

            Color OkunanRenk, DonusenRenk;

            Bitmap GirisResmi = new Bitmap(pictureBox1.Image);

            int ResimGenisligi = GirisResmi.Width;
            int ResimYüksekligi = GirisResmi.Height;

            Bitmap CikisResmi = new Bitmap(ResimGenisligi, ResimYüksekligi);
            

            int GriDeger = 0;
            for (int x = 0; x < ResimGenisligi; x++)
            {
                for (int y = 0; y < ResimYüksekligi; y++)
                {
                    OkunanRenk = GirisResmi.GetPixel(x, y);

                    double R = OkunanRenk.R;
                    double G = OkunanRenk.G;
                    double B = OkunanRenk.B;

                    GriDeger = Convert.ToInt16(R * 0.2125 + G * 0.7154 + B * 0.072);

                    DonusenRenk = Color.FromArgb(GriDeger, GriDeger, GriDeger); 
                    CikisResmi.SetPixel(x, y, DonusenRenk);
                }
            }
            return CikisResmi;
        }

        private void bT709YöntemiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = bT709Yöntemi(image);
            pictureBox2.Image = gri;

        }

        private Bitmap açıklıkDesaturationYöntemi(Bitmap cikisresmi)
        {

            for (int i = 0; i < cikisresmi.Height - 1; i++)
            {
                for (int j = 0; j < cikisresmi.Width - 1; j++)
                {
                    int deger = (cikisresmi.GetPixel(j, i).R + cikisresmi.GetPixel(j, i).G + cikisresmi.GetPixel(j, i).B) / 3;
                    Color renk;
                    renk = Color.FromArgb(deger, deger, deger);
                    cikisresmi.SetPixel(j, i, renk);
                }
            }
            return cikisresmi;

        }
        private void açıklıkDesaturationYöntemiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap image = new Bitmap(pictureBox1.Image);
            Bitmap gri = açıklıkDesaturationYöntemi(image);
            pictureBox2.Image = gri;
        }
    }
}
